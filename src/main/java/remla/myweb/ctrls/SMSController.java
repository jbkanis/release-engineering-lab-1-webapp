package remla.myweb.ctrls;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import mylib.RemlaUtil;
import remla.myweb.data.Sms;
import remla.myweb.data.SmsClassification;

@Controller
@RequestMapping(path = "/sms")
public class SMSController {

	private final RestTemplateBuilder rest;
	private final String modelHost;

	public SMSController(RestTemplateBuilder rest, Environment env) {
		modelHost = env.getProperty("MODEL_HOST");
		System.out.printf("Using '%s' as model host.\n", modelHost);
		this.rest = rest;
	}

	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("hostname", RemlaUtil.getHostName());
		return "sms/index";
	}

	@PostMapping("/")
	@ResponseBody
	public SmsClassification predict(@RequestBody Sms q) {

		String url = modelHost + "/predict";
		ResponseEntity<SmsClassification> res = rest.build().postForEntity(url, q, SmsClassification.class);

		return res.getBody();
	}
}